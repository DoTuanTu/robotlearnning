﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChooSceneController : MonoBehaviour
{
    [SerializeField] private Button[] goToLessonScene;
    [SerializeField] private Button backBTN;
    // Start is called before the first frame update
    void Start()
    {
       
            goToLessonScene[0].onClick.AddListener(delegate { GoToLesson(1); });
            goToLessonScene[1].onClick.AddListener(delegate { GoToLesson(2); });
            goToLessonScene[2].onClick.AddListener(delegate { GoToLesson(3); });
            goToLessonScene[3].onClick.AddListener(delegate { GoToLesson(4); });
            goToLessonScene[4].onClick.AddListener(delegate { GoToLesson(5); });
            goToLessonScene[5].onClick.AddListener(delegate { GoToLesson(6); });
            goToLessonScene[6].onClick.AddListener(delegate { GoToLesson(7); });
            goToLessonScene[7].onClick.AddListener(delegate { GoToLesson(8); });
            goToLessonScene[8].onClick.AddListener(delegate { GoToLesson(9); });
            goToLessonScene[9].onClick.AddListener(delegate { GoToLesson(10); });
            backBTN.onClick.AddListener(delegate { Back(); });
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GoToLesson(int index)
    {
        SceneManager.LoadScene("Lesson " + index.ToString());
    }
    public void Back()
    {
        SceneManager.LoadScene("HomeScene");
    }
}
