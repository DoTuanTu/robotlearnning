﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DemoControl : MonoBehaviour
{
    [SerializeField] private Button backBTN;
    // Start is called before the first frame update
    void Start()
    {
        backBTN.onClick.AddListener(delegate { Back(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Back()
    {
        SceneManager.LoadScene("HomeScene");
    }
}
