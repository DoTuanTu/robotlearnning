﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeSceneController : MonoBehaviour
{
    [SerializeField] private Button createBTN;
    [SerializeField] private Button learnBTN;
    [SerializeField] private Button vsBTN;
    // Start is called before the first frame update
    void Start()
    {
        learnBTN.GetComponent<Button>().onClick.AddListener(delegate { NextScene("ChooseScene"); });
        createBTN.GetComponent<Button>().onClick.AddListener(delegate { NextScene("Lesson 1"); });
        vsBTN.GetComponent<Button>().onClick.AddListener(delegate { NextScene("Vs"); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NextScene(string name)
    {
        SceneManager.LoadScene(name);
    }
}
