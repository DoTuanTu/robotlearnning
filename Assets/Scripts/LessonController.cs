﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LessonController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Button Blocking1;
    [SerializeField] private Button Blocking2;
    [SerializeField] private Button ConditionBlock;
    [SerializeField] private Button EvenBlock;
    [SerializeField] private Button ExecutionBlock;
    [SerializeField] private Button InfoBlock;
    [SerializeField] private Button Non_Blocking;
    [SerializeField] private Button BlocSettingBlockking1;
    [SerializeField] private Button goToDemo;
    [SerializeField] private Transform[] blockPrefab;

  
    Transform Clone;
    void Start()
    {
        Blocking1.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[0],Blocking1.gameObject.transform); });
        Blocking2.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[1], Blocking2.gameObject.transform); });
        ConditionBlock.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[2], ConditionBlock.gameObject.transform); });
        EvenBlock.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[3], EvenBlock.gameObject.transform); });
        ExecutionBlock.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[4], ExecutionBlock.gameObject.transform); });
        InfoBlock.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[5], InfoBlock.gameObject.transform); });
        Non_Blocking.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[6], Non_Blocking.gameObject.transform); });
        BlocSettingBlockking1.GetComponent<Button>().onClick.AddListener(delegate { CreateBlocks(blockPrefab[7], BlocSettingBlockking1.gameObject.transform); });
        goToDemo.GetComponent<Button>().onClick.AddListener(delegate { Demo(); });
      
       
    }

    // Update is called once per frame
    void Update()
    {
        

    }

    public void CreateBlocks(Transform blockPrefab,Transform pos)
    {
        Clone = Instantiate(blockPrefab, pos.position, Quaternion.identity);
    }
    public void Demo()
    {
        SceneManager.LoadScene("Demo");
    }
}
